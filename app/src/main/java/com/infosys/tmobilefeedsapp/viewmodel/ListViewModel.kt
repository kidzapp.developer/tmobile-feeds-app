package com.infosys.tmobilefeedsapp.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.infosys.tmobilefeedsapp.model.OffersApisService
import com.infosys.tmobilefeedsapp.model.Page
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers

class ListViewModel : ViewModel() {

    val offers: MutableLiveData<Page> = MutableLiveData()


    fun refresh() {
        getOffers()
    }

    private val disposable = CompositeDisposable()
    private val apiService = OffersApisService()

    private fun getOffers() {

        apiService.getOffers()
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeWith(object : DisposableSingleObserver<Page>() {

                override fun onSuccess(list: Page) {


                    offers.value = list
                }

                override fun onError(e: Throwable) {
                    e.printStackTrace()
                }


            })

    }

    override fun onCleared() {
        super.onCleared()
        disposable.clear()
    }
}