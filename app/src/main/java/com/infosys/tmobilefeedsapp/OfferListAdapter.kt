package com.infosys.tmobilefeedsapp

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.infosys.tmobilefeedsapp.model.Card
import com.infosys.tmobilefeedsapp.util.loadImage
import kotlinx.android.synthetic.main.item_feed.view.*


class OfferListAdapter(private val offersList: ArrayList<Card>) :
    RecyclerView.Adapter<OfferListAdapter.OfferViewHolder>() {

    class OfferViewHolder(var view: View) : RecyclerView.ViewHolder(view)


    fun updateOffersList(newOffersList: ArrayList<Card>) {
        offersList.clear()
        offersList.addAll(newOffersList)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OfferViewHolder {

        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.item_feed, parent, false)

        return OfferViewHolder(view)
    }

    override fun getItemCount() = offersList.size

    override fun onBindViewHolder(holder: OfferViewHolder, position: Int) {

        holder.view.offer_tilte.text = offersList[position].card?.title?.value
        holder.view.imageView.loadImage(offersList[position].card?.image?.url)

    }

}