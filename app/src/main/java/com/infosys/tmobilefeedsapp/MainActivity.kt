package com.infosys.tmobilefeedsapp

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.infosys.tmobilefeedsapp.model.Page
import com.infosys.tmobilefeedsapp.viewmodel.ListViewModel
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private lateinit var viewModel: ListViewModel
    private val listAdapter = OfferListAdapter(arrayListOf())


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        viewModel = ViewModelProvider.NewInstanceFactory().create(ListViewModel::class.java)
        viewModel.offers.observe(this, object : Observer<Page> {

            override fun onChanged(t: Page?) {

                t?.let { listAdapter.updateOffersList(it.page.cards) }
            }
        })

        viewModel.refresh()
        offerList.apply {

            layoutManager = LinearLayoutManager(context)
            adapter = listAdapter
        }
    }
}