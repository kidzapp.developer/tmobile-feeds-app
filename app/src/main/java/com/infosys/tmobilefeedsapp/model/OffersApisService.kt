package com.infosys.tmobilefeedsapp.model

import io.reactivex.Single
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class OffersApisService {
    private val BASE_URL = "https://private-8ce77c-tmobiletest.apiary-mock.com/"
    private val api = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .build()
        .create(OffersApi::class.java)


    fun getOffers(): Single<Page> {


        return api.getOffers()
    }
}