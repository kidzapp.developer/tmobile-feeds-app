package com.infosys.tmobilefeedsapp.model

import io.reactivex.Single
import retrofit2.http.GET

/**
 * Provision to add if there are multiple Api endpoints
 */
interface OffersApi {

    @GET("test/home")
    fun getOffers(): Single<Page>

}