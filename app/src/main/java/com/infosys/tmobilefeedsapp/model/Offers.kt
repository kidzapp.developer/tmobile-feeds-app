package com.infosys.tmobilefeedsapp.model

data class Page(
    val page: PageX
)

data class PageX(
    val cards: ArrayList<Card>
)

data class Attributes(
    val font: Font,
    val text_color: String
)

data class AttributesX(
    val font: FontX,
    val text_color: String
)

data class AttributesXX(
    val font: FontXX,
    val text_color: String
)


data class Card(
    val card: CardX?,
    val card_type: String
)

data class CardX(
    val attributes: Attributes,
    val description: Description,
    val image: Image,
    val title: Title,
    val value: String
)

data class Description(
    val attributes: AttributesX,
    val value: String
)

data class Font(
    val size: Int
)

data class FontX(
    val size: Int
)

data class FontXX(
    val size: Int
)

data class Image(
    val size: Size,
    val url: String
)

data class Offers(
    val page: PageX
)

data class Size(
    val height: Int,
    val width: Int
)

data class Title(
    val attributes: AttributesXX,
    val value: String
)
